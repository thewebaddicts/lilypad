<?php

namespace App\Http\Controllers;
use App\Models\AboutUsModel;
use App\Models\ContactInformationsModel;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public static function about()
    {
        $aboutus= AboutUsModel::where('cancelled', 0)->get();
        $contact_informations=ContactInformationsModel::where('cancelled', 0)->get();
        return view('/pages.about',['aboutus'=>$aboutus,'contact_informations'=>$contact_informations]);

    }


}
