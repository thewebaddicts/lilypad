<?php

namespace App\Http\Controllers;
use App\Models\ArticlesModel;
use App\Models\ContactInformationsModel;
use Illuminate\Http\Request;

class ArticlesPageController extends Controller
{
    public static function infos ()
    {

        $articles = ArticlesModel ::where('cancelled', 0)->get();
        $contact_informations=ContactInformationsModel::where('cancelled', 0)->get();
        return view('/pages.article',['articles'=>$articles,'contact_informations'=>$contact_informations]);

    }

}
