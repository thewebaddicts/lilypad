<?php

namespace App\Http\Controllers;

use App\Models\Contact_UsModel;
use App\Models\ContactInformationsModel;
use App\Models\EmailTemplateModel;
use App\Mail\emailtemplate;
use Illuminate\Support\Facades\Mail;
use App\Models\Message;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function request(Request $request)
    {

        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',


            'message' => 'required'
        ]);

        $form = new Contact_UsModel;
        $form->first_name = request()->input('first_name');
        $form->last_name = request()->input('last_name');
        $form->email = request()->input('email');
        $form->phone_number = request()->input('phone_number');
        $form->message = request()->input('message');
        $form->save();



       
            $dictionary = [
                    "First Name"=> $request->input('first_name'),
                    "Last Name" => $request->input('last_name'),
                    "Email" => $request->input('email'),
                    "Phone" => $request->input('phone'),
                    "Message" => $request->input('message'),


                
            ];

            $template = EmailTemplateModel::where('cancelled', 0)->where('location', 'request_newsletter')->first();
            try {

                $company_email = explode(',', $template->email);
                Mail::to($company_email)->send(new emailtemplate('admin form submit', $dictionary));
                }catch (\Throwable $th){
        //        dd($th);
                }
                try {
                Mail::to(request()->input('email'))->queue(new emailtemplate('client form submit', $dictionary));
                }catch (\Throwable $th){
        //            dd($th);
                }
        
        return redirect()->route('home', ["notification_id" => 1]);


//       return redirect()->back();
//        return redirect()->back()->with('notification_id', 1);

    }

    public static function cinformations()
    {
        $contact_informations = ContactInformationsModel::where('cancelled', 0)->get();

        //    dd($servicebyid);

        return view('pages.contactus', ['contact_informations' => $contact_informations]);
    }


}
