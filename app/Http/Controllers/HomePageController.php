<?php

namespace App\Http\Controllers;
use App\Models\AboutUsModel;
use App\Models\ContactInformationsModel;
use App\Models\ProductsModel;
use App\Models\SlideShowModel;
use App\Models\SlideShow_BodyModel;
use App\Models\BlogsModel;


use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public static function render()
    {

        $slideshow = SlideShowModel::where('cancelled', 0)->get();
        $aboutus= AboutUsModel::where('cancelled', 0)->get();
        $products = ProductsModel::where('cancelled', 0)->get();
        $slideshowbody = SlideShow_BodyModel
            ::where('cancelled', 0)->get();
        $blog = BlogsModel::where('cancelled', 0)->first();
        try{
            $es = json_decode($blog->blogs) ;
        }catch(\Exception $e){
            $es = [];
        }

        $contact_informations=ContactInformationsModel::where('cancelled', 0)->get();
//$services =servicesmodel::where('cancelled', 0)->get();
//        $clients =clientsmodel::where('cancelled', 0)->get();
//        $process =processmodel::where('cancelled', 0)->get();
//        $servicespage= servicespagemodel::where('cancelled', 0)->get();
        // $servicebyid= servicespagemodel::where('cancelled', 0)->first();



        return view('/pages.welcome',['slideshow'=>$slideshow,'aboutus'=>$aboutus,'products'=>$products,'slideshowbody'=>$slideshowbody,'blog'=>$blog,'contact_informations'=>$contact_informations , 'es'=>$es]);

    }
//    public static function display_services($id){
//        $servicebyid= servicespagemodel::where('cancelled', 0)->where('id',$id)->first();
//        $servicespage= servicespagemodel::where('cancelled', 0)->get();
//        //    dd($servicebyid);
//
//        return view('/services',['servicebyid'=>$servicebyid,'servicespage'=>$servicespage]);
//    }
//    public static function article()
//    {
//
//        $blogs = BlogsModel::where('cancelled', 0)->get();
//         return view('/pages.welcome',['blogs'=>$blogs]);
//
//    }
}
