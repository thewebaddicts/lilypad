<?php

namespace App\Http\Controllers;
use App\Models\ContactInformationsModel;
use App\Models\OurProductsModel;
use Illuminate\Http\Request;

class OurProductsController extends Controller
{
    public static function display_products() {
        $ourproducts=OurProductsModel::where('cancelled', 0)->get();
        $contact_informations=ContactInformationsModel::where('cancelled', 0)->get();

        //    dd($servicebyid);

        return view('pages.product',['ourproducts'=>$ourproducts,'contact_informations'=>$contact_informations]);
    }
}
