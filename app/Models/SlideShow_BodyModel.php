<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SlideShow_BodyModel extends Model
{
    use HasFactory;
    protected $table='slideshow_body';

    public function getImageAttribute(){
        return env('DATA_URL')."/topimage/".$this->id.".".$this->extension_image."?v=.".$this->version;
    }
}
