function ShowMessage(Title, Text) {
    $.fancybox.open(
        '<div class="message"><h2 style="letter-spacing: 2px; padding-top:20px;">' +
        Title +
        "</h2><p>" +
        Text +
        "</p></div>"
    );
}

var isMobile = false; //initiate as false
// device detection
if (
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
        navigator.userAgent
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        navigator.userAgent.substr(0, 4)
    )
) {
    isMobile = true;
}


function closeFancyBox() {
    parent.jQuery.fancybox.getInstance().close();
}

function InitializeMenuScroll() {
    var scroll_start = 0;
    var startchange = $(".menu-marker");
    var offset = startchange.offset();

    if (startchange.length > 0 && $(document).scrollTop() - offset.top > 0) {
        $(".navbar-609").addClass("compact");
    } else {

        $(".navbar-609").removeClass("compact");
    }

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


function InitializeMenuScroll2() {
    var scroll_start = 0;
    var about_instructions = 1;
    var startchange2 = $(".close-about-instruction-popup");
    var offset = startchange2.offset();


    if (startchange2.length > 0 && $(document).scrollTop() - offset.top > 0) {


        if (about_instructions == 1) {

            sleep(0).then(() => {
                $(".screen-1").css("height", "100%");
                $(".screen-1").css("width", "100%");

            });

            $("#switcher-tv-1").click(function () {
                $(".screen-content-1").removeClass("glitch-text");
                $(".screen-content-1").addClass("loading-back-home");
                sleep(3000).then(() => {
                    about_instructions++;
                    if (about_instructions >= 2) {
                        $(".screen-1").css("display", "none");
                        sessionStorage.setItem("popState", "shown");
                    }
                });
            });

        }


    }
    console.log(about_instructions);

}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function changeQueryParameter(key, value, location = null) {
    var uri = window.location.href;

    var location_str = location ? "#" + location : "";

    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf("?") !== -1 ? "&" : "?";
    if (uri.match(re)) {
        //return uri.replace(re, "$1" + key + "=" + value + "$2");
        window.location.href =
            uri.replace(re, "$1" + key + "=" + value + "$2") + location_str;
    } else {
        window.location.href =
            uri + separator + key + "=" + value + location_str;
    }
}

function addQueryParamsWithoutReloading(key, value) {
    var documentTitle = document.title;
    var uri = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf("?") !== -1 ? "&" : "?";
    if (uri.match(re)) {
        var obj = {
            Title: documentTitle,
            Url: uri.replace(re, "$1" + key + "=" + value + "$2"),
        };
    } else {
        var obj = {
            Title: documentTitle,
            Url: uri + separator + key + "=" + value,
        };
    }
    history.pushState(obj, obj.Title, obj.Url);
}

function redirectWithQueryParams(url) {
    str = "";
    var obj = getParams();
    Object.keys(obj).forEach((key, index) => {
        str +=
            index == 0
                ? "?" + key + "=" + obj[key]
                : "&" + key + "=" + obj[key];
    });
    return (window.location.href = url + str);
}

function getParams() {
    var url = window.location.href;
    if (url.indexOf("?") == -1) {
        return {};
    }
    var params = {};
    var parser = document.createElement("a");
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
}

function updateQueryStringParameter(key, value) {
    var uri = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf("?") !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return (window.location.href = uri.replace(
            re,
            "$1" + key + "=" + value + "$2"
        ));
    } else {
        return (window.location.href = uri + separator + key + "=" + value);
    }
}

function removeQueryNotificationString() {
    var documentTitle = document.title;
    var urlSplit = window.location.href.split("?");
    var obj = {Title: documentTitle, Url: urlSplit[0]};
    history.pushState(obj, obj.Title, obj.Url);
}

function playVideo() {
    $('.intro-video').parent().click(function () {
        if ($(this).children(".intro-video").get(0).paused) {
            $(this).children(".intro-video").get(0).play();
            $(this).children(".icon-play").fadeOut();
        } else {
            $(this).children(".intro-video").get(0).pause();
            $(this).children(".icon-play").fadeIn();
        }
    });
}

function toggleFrontBack() {

    $(".contact-details-button").click(function () {
        $(this).parents('.container-blocks').addClass('active-flip');
        $(this).parents('.contact-block').css('z-index', 10);
        $(this).parents('.location-block').css('z-index', 9);
        if ($(this).parents('.container-blocks').hasClass('flip-back')) {
            $(this).parents('.container-blocks').removeClass('flip-back');
        }
    });
    $(".back-btn").click(function () {
        $(this).parents('.container-blocks').addClass('flip-back');
        $(this).parents('.location-block').css('z-index', 10);
        $(this).parents('.contact-block').css('z-index', 9);
        if ($(this).parents('.container-blocks').hasClass('active-flip')) {
            $(this).parents('.container-blocks').removeClass('active-flip');
        }
    });
}

function deleteInput() {
    if ($('#input-email-sub').val().length == 0) {
        $('.delete-input-text').css('display', 'none');

    } else {
        $('.delete-input-text').css('display', 'block');
    }
}

function postNewsletter() {
    var ajaxLoading = true;
    $('.submit-email-news').click(function (e) {
        e.preventDefault();

        var csrf = $("meta[name=csrf-token]").attr("content");
        var email_sub = $('#input-email-sub').val();


        if (ajaxLoading) {
            if (email_sub) {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email_sub)) {
                    $('.subscribe-button').hide();
                    $('.email-text-input').hide();
                    $('.loader-form').show();

                    $.ajax({
                        type: "POST",
                        url: '/sub-request',
                        data: {_token: csrf, email_sub: email_sub},

                        success: function (data) {
                            if (data == 1) {

                                $('.loader-form').hide();
                                $('.complete-form').show();
                                ajaxLoading = false;
                            }
                        }

                    })
                }
            }
        }
    });
}

function postContactRequest() {
    var ajaxLoading = true;

    $('.submit-contact-request').click(function (e) {
        e.preventDefault();

        var csrf = $("meta[name=csrf-token]").attr("content");
        var f_name = $('#input-contact-request-first_name').val();
        var l_name = $('#input-contact-request-last_name').val();
        var email_address = $('#input-contact-request-email_address').val();
        var phone_country_code = $('#input-contact-request-country_code').val();
        var phone_number = $('#input-contact-request-phone_number').val();
        var phone = phone_country_code + phone_number;
        var msg = $('#input-contact-request-text').val();


        if (ajaxLoading) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email_address)) {
                if (f_name && l_name && email_address && phone) {
                    $('.submit-contact-request').hide();
                    $('.loader-contact-form').show();
                    $.ajax({
                        type: "POST",
                        url: '/contact-request',
                        data: {
                            _token: csrf, f_name: f_name, l_name: l_name,
                            email_address: email_address, phone: phone, msg: msg
                        },
                        success: function (data) {
                            if (data == 1) {
                                $('.loader-contact-form').hide();
                                $('.complete-contact-form').show();
                                ajaxLoading = false;
                            }
                        }


                    })
                }
            }
        }
    });

}

function serviceInput() {
    var data_vals = [];
    $(".image-service-block-choose").each(function () {
        var $this = $(this);

        if ($this.hasClass('active')) {
            var data_val = String($this.data("id"));
            data_vals.push('"' + data_val + '"');
            $('#input-quote-request-service_id').val(data_vals);
            $this.css('border', '2px solid' + ' ' + $this.attr('data-color'))

        } else {
            var data_val_pop = String($this.data("id"));
            data_vals = data_vals.filter(e => e !== '"' + data_val_pop + '"');
            $('#input-quote-request-service_id').val(data_vals);
            $this.css('border', '2px solid transparent')
        }
    });

    $('.image-service-block-choose').on('click', function () {
        var $this = $(this);
        $this.toggleClass('active');
        if ($this.hasClass('active')) {
            var data_val = String($this.data("id"));
            data_vals.push('"' + data_val + '"');
            $('#input-quote-request-service_id').val(data_vals);
            $this.css('border', '2px solid' + ' ' + $this.attr('data-color'))
        } else {
            var data_val_pop = String($this.data("id"));
            data_vals = data_vals.filter(e => e !== '"' + data_val_pop + '"');
            $('#input-quote-request-service_id').val(data_vals);
            $this.css('border', '2px solid transparent')
        }
    });
}

function keyVal() {
    $('.key_val').on('click', function () {
        var $this = $(this);
        var subId = $this.attr('data-sub');
        if ($this.hasClass('active')) {
            $('.key_val').removeClass('active');
            $('.contents').show();
            $('.all_val').addClass('active');
        } else {
            $('.all_val').removeClass('active');
            $('.key_val').removeClass('active');
            $this.addClass('active');
            $('.contents').hide();
            $('.contents[data-sub="' + subId + '"]').show();
        }
    });
}


function switched(switchid) {

    $('.product').hide();
    $('.prod').hide()

    $('#' + switchid).show();


}

function switching(switchid) {
    $('.prod').show()
    $('#' + switchid).show()
}

function appaer() {
    $('.switcheproduct').click(function () {
        $('.switcheproduct').removeClass('active');
        $(this).toggleClass('active');
    });


}

// function showw(){
//     $('.product').click(function(){
//         $('.product').removeClass('show');
//         $(this).toggleClass('show');
//     });
// }



