var version = document.currentScript.getAttribute("attr-cache-version");

function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

loadCss("/css/app.css?v=" + version);
loadCss("/css/bootstrap.min.css");
loadCss("https://pro.fontawesome.com/releases/v5.10.0/css/all.css");
loadCss("/js/swiper/swiper-bundle.min.css");
loadCss("/js/jquery.fancybox/dist/jquery.fancybox.min.css");
loadCss("https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css");



requirejs.config({
    waitSeconds: 200,
    paths: {
        functions: "/js/functions.js?v=" + version,
        jquery: "/js/jquery/dist/jquery.min.js?v=" + version,
        fancybox:
            "/js/jquery.fancybox/dist/jquery.fancybox.min.js?v=" + version,
        swipejs: "/js/swiper/swiper-bundle.min.js?v=" + version,
        aos: "https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos",
    },

    shim: {
        functions: {
            deps: ["jquery", "fancybox"],
        },
        fancybox: {
            deps: ["jquery"],
        },

        swipejs: {
            deps: ["jquery"],
        },
        aos: {
            deps: ["jquery"],
        },

    },
});


//Define dependencies and pass a callback when dependencies have been loaded
require(["jquery"], function ($) {
    jQuery.event.special.touchstart = {
        setup: function (_, ns, handle) {
            this.addEventListener("touchstart", handle, {passive: true});
        },
    };


    $('.burger').click(function () {
        $(this).toggleClass('active-burger')
    });


    $('form input[type="submit"],form button[type="submit"], form button').on('click', function () {

        $(this).parents('form').addClass('recheck')
    });

    $('.delete-input-text').click(function () {
        $('#input-email-sub').val('');
        $(this).hide();
    });



    $.fn.slideFadeToggle = function (speed, easing, callback) {
        return this.animate(
            {opacity: "toggle", height: "toggle"},
            speed,
            easing,
            callback
        );
    };

    $("#toTop").click(function () {
        $("html, body").animate({scrollTop: 0}, 0);
    });

    try {
        //we are not sure this function exists

        FooterFunctions();
    } catch (e) {
    }
});


require(["functions"], function () {

    window.addEventListener("scroll", InitializeMenuScroll);
    InitializeMenuScroll();


    try {
        NotificationFunction();
        playVideo();
        deleteInput();
        postNewsletter();
        keyVal();
        serviceInput();
        toggleFrontBack();


        // autoScroll();
    } catch (e) {
        console.log(e);
    }
    try{
        switched();
    }
    catch(e) {
        console.log(e);
    }
    try{
        appaer()
    }
    catch(e) {
        console.log(e);
    }
    try{
        switching()
    }
    catch(e) {
        console.log(e);
    }
    try {
        NotificationFunction();
    }
        catch(e){
            console.log(e);
        }

});

require(["aos"], function (AOS) {
    AOS.init({easing: "ease-in-out-sine", duration: 500, once: false, mirror: true});
});





require(["swipejs"], function (Swiper) {


    var swiper_products = new Swiper(".swiper-slideshow", {
        // Optional parameters
        direction: "horizontal",
        slidesPerView: "auto",
        loop: false,
        spaceBetween: 0,
        autoplay:false,



        breakpoints: {
            1199: {
                slidesPerView:3,
                spaceBetween: 40,
            },
            651: {
                slidesPerView: 1.5,
                spaceBetween: 20
            },
            900: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            993: {
                slidesPerView: 2.5,
                spaceBetween: 80
            },
            320: {
                slidesPerView: "auto",
                spaceBetween: 20
            },

        },
        pagination: {
            el: ".slideshow-pagination",
            clickable: true,
        },
        // navigation: {
        //     nextEl: ".next-product",
        //     prevEl: ".prev-product",
        // },
        grabCursor: true,
    });
});

require(["fancybox"], function () {
    $(".fancybox").fancybox({
        helpers: {
            title: "HELLO",
            thumbs: {
                width: 50,
                height: 50,
            },
        },
    });
    $(".fancybox-media").fancybox({
        openEffect: "none",
        closeEffect: "none",
        helpers: {
            media: {},
        },
    });

    $.fancyConfirm = function (opts) {
        opts = $.extend(
            true,
            {
                title: "Are you sure?",
                message: "",
                okButton: "OK",
                noButton: "Cancel",
                callback: $.noop,
            },
            opts || {}
        );

        $.fancybox.open({
            type: "html",
            src:
                '<div class="fc-content">' +
                "<h3>" +
                opts.title +
                "</h3>" +
                "<p>" +
                opts.message +
                "</p>" +
                '<div class="text-right confirmation">' +
                '<a data-value="0" data-fancybox-close>' +
                opts.noButton +
                "</a>" +
                '<button data-value="1" data-fancybox-close class="rounded">' +
                opts.okButton +
                "</button>" +
                "</div>" +
                "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl:
                    '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' +
                    '<div class="fancybox-bg"></div>' +
                    '<div class="fancybox-inner">' +
                    '<div class="fancybox-stage"></div>' +
                    "</div>" +
                    "</div>",
                afterClose: function (instance, current, e) {
                    var button = e ? e.target || e.currentTarget : null;
                    var value = button ? $(button).data("value") : 0;

                    opts.callback(value);
                },
            },
        });
    };
});
