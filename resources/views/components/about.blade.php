
<div id="aboutus"></div>
<div class="home-body1">
    <div class="leftimage">
        <img src="/assets/images/leftbackground1.png" alt="">
    </div>
    <div class="lilyimage">
        <img src="/assets/images/lilyimage.png" data-aos="fade-up" alt="">
    </div>
    <div class="content-fluid">

        @if (isset($aboutus) && count($aboutus) > 0)
            @foreach ($aboutus as $aboutus)
{{--                                @dd($aboutus)--}}
                @php $a = json_decode($aboutus->details) @endphp
                {{--            @dd($a)--}}
                <div class="whoweareparagraphs" >
                    @foreach ($a as $a)
                        @if ($loop->index < 2)
                            <div class="whowearetitle" >{{ $a->title }}</div>
                            <div class="whowearedetails" data-aos="fade-up">{!! $a->text !!}</div>
                            {{--                @dd(Request::url())--}}
{{--                            @if (Request::url()=='https://lilypad.twalab.com')--}}
{{--                            @if (Request::url()=='https://lilypadv2.test')--}}
{{--                                <a href="{{ $a->button_link }}" style="text-decoration:none">--}}
{{--                                    <div class="info_button">{{ $a->button_text }} <span> <img--}}
{{--                                                src="/assets/svg/Icon feather-arrow-right.svg" alt=""></span></div>--}}
{{--                                </a>--}}
{{--                            @endif--}}
                        @endif
                    @endforeach
                    <div class="fouanicompany" ><div class="title">Manufactured by fouani Nigeria limited</div> <div class="logo" ><img
                                src="/assets/svg/LOGO.svg" alt=""></div></div>
                    <a href="{{ $aboutus->fouani_link }} " style="text-decoration:none">
                        <div class="fouani_button">{{ $aboutus->fouani_button }} <span> <img src="/assets/svg/Icon feather-arrow-right.svg"
                                                                         alt=""></span></div>
                    </a>
                </div>

            @endforeach
        @endif
        {{--        <div class="whoweareparagraphs">--}}
        {{--            <div class="whowearetitle">Who We Do</div> <br>--}}
        {{--            <div class="whowearedetails">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est ultricies integer quis. Iaculis urna id volutpat lacus laoreet. Mauris vitae ultricies leo integer malesuada. Ac odio tempor orci dapibus ultrices in.<br> <br>--}}

        {{--                Egestas diam in arcu cursus euismod. Dictum fusce ut placerat orci nulla. Tincidunt ornare massa eget egestas purus viverra accumsan in nisl. Tempor id eu nisl nunc mi ipsum faucibus. Fusce id velit ut tortor pretium. Massa ultricies mi quis hendrerit dolor magna eget. Nullam eget felis eget nunc lobortis. Faucibus ornare suspendisse sed nisi. Sagittis eu volutpat odio facilisis mauris sit amet massa.<br> <br>--}}

        {{--                Egestas diam in arcu cursus euismod. Dictum fusce ut placerat orci nulla. Tincidunt ornare massa eget egestas purus viverra accumsan in nisl. Tempor id eu nisl nunc mi ipsum faucibus.</div>--}}
        {{--            <div class="info_button">Readmore</div>--}}

        {{--        </div>--}}


    </div>
    <div class="rightimage">
        <img src="/assets/images/righttopbackground.png" alt="">
    </div>
</div>
<div class="space" style="height:75px"></div>





