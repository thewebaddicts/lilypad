<div class="burger">
    <i class="fas fa-bars fa-2x"></i>
    <i class="fas fa-times fa-2x"></i>
</div>

<nav class="navbar">
    <ul class="nav-links">
        <li class="nav-link"><a href="/#aboutus">About-Us</a></li>
        <li class="nav-link"><a href="{{route('our-products')}}">Our Products</a></li>
        <li class="nav-link"><a href="{{ route('blogs') }}">Blogs</a></li>
        <li class="nav-link"><a href="{{ route('contact') }}">Contact-Us</a></li>


    </ul>
</nav>
