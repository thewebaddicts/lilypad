<?php $PhoneCodes = ecom('countries')->getPhoneCodes(); ?>
<div class="home-body5">
    @if (isset($contact_informations) && count($contact_informations) > 0)
        @foreach ($contact_informations as $informations)
            {{--            @dd($informations)--}}
            <div class="content-fluid">

                <div class="column1">
                    <div class="contactustitle">Contact Us</div>
                    <div class="numberemailinfo">
                        <a href="tel:+234 1 888 4444" style="text-decoration: none">
                            <div class="number">Call us: {{ $informations['phone_number'] }}</div>
                        </a>
                        <a href="mailto: Info@lilypad.com" style="text-decoration: none">
                            <div class="email">email: {{ $informations['email'] }}</div>
                        </a>

                    </div>
                    <form method="POST" action="/contact">
                        @csrf
                        <div class="fullname">
                            <div class="firstname">

                                <div class="fnam">First Name</div>
                                <input type="text" name="first_name" value="{{ old('first_name') }}"
                                       class=" input-field"
                                       placeholder="First Name ..." required/>
                                @error('first_name')
                                <span style="color: red ;font-size: 11px">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="lastname">
                                <div class="lnam">Last Name</div>
                                <input type="text" name="last_name" value="{{ old('last_name') }}" class=" input-field"
                                       placeholder="Last Name ..." required/>
                                @error('last_name')
                                <span style="color: red ;font-size: 11px">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="fullname">
                            <div class="firstname">

                                <div class="fnam">Email Address</div>
                                <input type="text" name="email" value="{{ old('email') }}" class=" input-field"
                                       placeholder="Email address..." required/>
                                @error('email')
                                <span style="color: red ;font-size: 11px">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="lastname">

                                <div class="fnumb">Phone number</div>
                                <div class="code">
                                    <select name="phone_country_code" id="">
                                        @foreach($PhoneCodes AS $code)
                                            <option @if($code['code'] === 'NG') selected
                                                    @endif value="{{ $code['code'] }}">
                                                +{{ $code['phone_code'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <input type="text" name="phone_number" value="{{ old('phone_number') }}"
                                           class=" input-field"
                                           placeholder="Phone Number ..." required/>
                                    @error('phone_number')
                                    <span style="color: red ;font-size: 11px">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="message">
                            <div class="messagetitle">Message</div>

                            <textarea type="text" name="message" value="{{ old('message') }}" class=" input-field1"
                                      placeholder="Your Message..."></textarea>
                            @error('message')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="sub">
                            <input type="submit" value="Send">
                        </div>
                    </form>
                </div>

                <div class="column2">
                    <div class="lilyendimage">
                        <img src="/assets/images/lilyendimage.png" alt="">
                    </div>
                </div>


            </div>
        @endforeach
    @endif
</div>
