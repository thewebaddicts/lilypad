<div class="footer">
    @if (isset($contact_informations) && count($contact_informations) > 0)
        @foreach ($contact_informations as $informations)
    <div class="content-fluid">
        <div class="devv">© {{ date('Y') }} LilyPad - Web Design and Development by <a
                href="https://thewebaddicts.com/"> The Web Addicts</a>
        </div>
        <div class="dev">
            <ul>
                <a href="{{ route('t&c') }}" style="text-decoration: none"><li>Terms & Conditions</li></a>
                <a href="{{ route('p&p') }}"style="text-decoration: none"><li style=>Privacy Policy</li></a>
                <a href="{{ route('c&p') }}"style="text-decoration: none"><li>Cookies Policy</li></a>

            </ul>

        </div>
        <div class="callussocial">
            <a href="tel:+234 1 888 4444"style="text-decoration: none"><div class="callus">Call us: {{ $informations['phone_number'] }}</div></a>
            <a href="{{ $informations['facbook_link'] }}"target="_blank">
                <div class="facebook">@include('includes.facebook')</div>
            </a>
            <a href="{{ $informations['instagram_link'] }}"target="_blank">
                <div class="insta">@include('includes.insta')</div>
            </a>


        </div>
    </div>
        @endforeach
        @endif
</div>
