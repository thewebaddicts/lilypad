<nav class="main">
    @if (isset($contact_informations) && count($contact_informations) > 0)
        @foreach ($contact_informations as $informations)
    <div class="content-nav-main content-fluid">
        <picture class="logo">
            <a href="{{ route('home') }}"><img src="assets/svg/logo-main.svg" alt="">
            </a>
        </picture>

        <div class="menu-main">
            <ul>
                <a href="/#aboutus"> <li>About-Us</li></a>
                <a href="{{ route('our-products') }}"> <li>Our Products</li></a>
                <a href="{{ route('blogs') }}"> <li>Blog</li></a>
                <a href="{{ route('contact') }}"> <li>Contact Us</li></a>

            </ul>
            <div class="social">
                <a href="{{ $informations['facbook_link'] }}" target="_blank"> @include('includes.facebook')</a>
                <a href="{{ $informations['instagram_link'] }}" target="_blank">@include('includes.insta')</a>

            </div>
        </div>
    </div>
        @endforeach
    @endif
</nav>

<script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
<script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>

