<svg xmlns="http://www.w3.org/2000/svg" width="36.5" height="36.5" viewBox="0 0 36.5 36.5">
    <g id="Component_10_1" data-name="Component 10 – 1" transform="translate(0.75 0.75)">
        <circle id="Ellipse_2" data-name="Ellipse 2" cx="17.5" cy="17.5" r="17.5"  stroke-width="1.5"/>
        <path id="Icon_awesome-facebook-f" data-name="Icon awesome-facebook-f" d="M11.739,11.384,12.3,7.722H8.786V5.345a1.831,1.831,0,0,1,2.065-1.979h1.6V.247A19.484,19.484,0,0,0,9.613,0C6.719,0,4.827,1.754,4.827,4.93V7.722H1.609v3.663H4.827v8.854h3.96V11.384Z" transform="translate(10.9 6.996)"  fill="none"/>
    </g>
</svg>
{{--  stroke="#fff"--}}
