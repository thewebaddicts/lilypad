<svg xmlns="http://www.w3.org/2000/svg" width="36.5" height="36.5" viewBox="0 0 36.5 36.5">
    <g id="Component_11_3" data-name="Component 11 – 3" transform="translate(0.75 0.75)">
        <circle id="Ellipse_1" data-name="Ellipse 1" cx="17.5" cy="17.5" r="17.5"  stroke-width="1.5"/>
        <g id="Icon_feather-instagram" data-name="Icon feather-instagram" transform="translate(8.836 8.949)">
            <path id="Path_6" data-name="Path 6" d="M7.3,3h8.607a4.3,4.3,0,0,1,4.3,4.3v8.607a4.3,4.3,0,0,1-4.3,4.3H7.3a4.3,4.3,0,0,1-4.3-4.3V7.3A4.3,4.3,0,0,1,7.3,3Z" transform="translate(-3 -3)"  stroke-linecap="round" stroke-linejoin="round" stroke-width="1.7"/>
            <path id="Path_7" data-name="Path 7" d="M18.281,14.6a3.125,3.125,0,1,1-2.633-2.633A3.125,3.125,0,0,1,18.281,14.6Z" transform="translate(-6.543 -6.493)"  stroke-linecap="round" stroke-linejoin="round" stroke-width="1.7"/>
            <path id="Path_8" data-name="Path 8" d="M26.25,9.75h0" transform="translate(-12.909 -5.877)"  stroke-linecap="round" stroke-linejoin="round" stroke-width="1.7"/>
        </g>
    </g>
</svg>
