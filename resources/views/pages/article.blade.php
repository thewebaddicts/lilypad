@extends('layouts.main')
@php error_reporting(E_ALL);
ini_set('display_errors' , 1); @endphp
@section('title')
    <title> HOME PAGE || {{ env('WEBSITE_NAME') }}</title>
@endsection
@section('content')

    @include('components.mainnav')
    @if (isset($articles) && count($articles) > 0)
        @foreach ($articles as $article)
{{--            @dd($article)--}}
    <div class="articleheader" style=" background-image: url('{{ env('DATA_URL') }}/articleimage/{{ $article->id }}.{{ $article->extension_topbackgroundimage}}?v={{ $article->version }}')" >
{{--                    @dd( $article->topbackgroundimage)--}}
    <div class="articleoverlay"></div>
        <div class="content-fluid">
            <div class="articletitle">
                {{ $article['article_name'] }}
            </div>

        </div>
    </div>
    <div class="articlebody1">
        <div class="content-fluid">
{{--            <div class="socialsvg">--}}
{{--                <div class="insta"></div>--}}
{{--                <div class="facebook"></div>--}}
{{--                <div class="twitter"></div>--}}
{{--            </div>--}}
            <div class="articleparagraphtop">
                {!! $article['top_paragraph'] !!}
            </div>
            <div class="paragraphtitle">
                {{ $article['first_paragraph_title'] }}
            </div>
        <div class="articleparagraph1">
             {!! $article['first_paragraph'] !!}
               </div>
            <div class="articleimagebody">
                <img src="{{ env('DATA_URL') }}/articleimages/{{ $article->id }}.{{ $article->extension_bodybackgroundimage}}?v={{ $article->version }}" alt="">
{{--                                    @dd( $article->bodybackgroundimage)--}}
            </div>
            <div class="paragraphtitle">
                {{ $article['second_paragraph_title'] }}
            </div>
            <div class="articleparagraph1">
                {!! $article['second_paragraph'] !!}
            </div>
            <div class="paragraphtitle">
                {{ $article['third_paragraph_title'] }}
            </div>
            <div class="articleparagraph1">
                {!! $article['third_paragraph'] !!}
            </div>




        </div>
    </div>
        @endforeach
    @endif








    @include('components.footer')
@endsection
