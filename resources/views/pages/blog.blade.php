@extends('layouts.main')
@php error_reporting(E_ALL);
ini_set('display_errors' , 1); @endphp
@section('title')
    <title> BLOGS || {{ env('WEBSITE_NAME') }}</title>
@endsection
@section('content')
    <div class="content-fluid">@include('components.bmenu')</div>
    @include('components.mainnav')
    <div class="blogspage">
    <div class="content-fluid">
        <div class="index">
            <div class="route"><span>Home</span> / Blog</div>
            <div class="title">Blog</div>
        </div>
        <div class="blogs">
            @if (isset($blogs) && count($blogs) > 0)
                @foreach ($blogs as $blog)
                    {{--                    @dd($blog)--}}
                    @php $e = json_decode($blog->blogs) @endphp
{{--                                        @dd($e)--}}
                    @foreach ($e as $e)
                        @if ($loop->index < 20)
                            <div class="diffrentblogs" data-aos="fade-up">
                                <div class="imageblog">
                                    <img src="{{ env('DATA_URL') . $e->image}}" alt="">
                                </div>
                                <div class="title">
                                    {{ $e->title }}
                                </div>
                                <div class="details">
                                    {!! $e->details !!}
                                </div>
                                <a style="text-decoration: none" href="{{ $e->button_link}}">
                                    <div class="info-button">
                                        {{ $e->button_text }}
                                        <span> <img
                                                src="/assets/svg/Icon feather-arrow-right.svg" alt=""></span>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            @endif
            {{--            <div class="diffrentblogs">--}}
            {{--                <div class="imageblog">--}}
            {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
            {{--                </div>--}}
            {{--                <div class="title">--}}
            {{--                    Article Name 1--}}
            {{--                </div>--}}
            {{--                <div class="details">--}}
            {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
            {{--                </div>--}}
            {{--                <div class="info-button">--}}
            {{--                    read more--}}

            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="diffrentblogs">--}}
            {{--                <div class="imageblog">--}}
            {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
            {{--                </div>--}}
            {{--                <div class="title">--}}
            {{--                    Article Name 1--}}
            {{--                </div>--}}
            {{--                <div class="details">--}}
            {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
            {{--                </div>--}}
            {{--                <div class="info-button">--}}
            {{--                    read more--}}

            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="diffrentblogs">--}}
            {{--                <div class="imageblog">--}}
            {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
            {{--                </div>--}}
            {{--                <div class="title">--}}
            {{--                    Article Name 1--}}
            {{--                </div>--}}
            {{--                <div class="details">--}}
            {{--                    hLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
            {{--                </div>--}}
            {{--                <div class="info-button">--}}
            {{--                    read more--}}

            {{--                </div>--}}
            {{--            </div>--}}


        </div>
        {{--        <div class="blogs">--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    hLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}


        {{--        </div>--}}
        {{--        <div class="blogs">--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    hLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}


        {{--        </div>--}}
        {{--        <div class="blogs">--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="diffrentblogs">--}}
        {{--                <div class="imageblog">--}}
        {{--                    <img src="/assets/images/blogsimage.png" alt="">--}}
        {{--                </div>--}}
        {{--                <div class="title">--}}
        {{--                    Article Name 1--}}
        {{--                </div>--}}
        {{--                <div class="details">--}}
        {{--                    hLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.--}}
        {{--                </div>--}}
        {{--                <div class="info-button">--}}
        {{--                    read more--}}

        {{--                </div>--}}
        {{--            </div>--}}


        {{--        </div>--}}
    </div>
    </div>
    <div class="emptydiv" style="height:50px"></div>
    @include('components.footer')



@endsection
