@extends('layouts.main')
@php error_reporting(E_ALL);
ini_set('display_errors' , 1); @endphp
@section('title')
    <title> Contact us || {{ env('WEBSITE_NAME') }}</title>
@endsection
@section('content')
    <div class="content-fluid">@include('components.bmenu')</div>
    @include('components.mainnav')
    @include('components.contact')
    @include('components.footer')
@endsection
