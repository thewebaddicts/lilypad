@extends('layouts.main')
@php error_reporting(E_ALL);
ini_set('display_errors' , 1); @endphp
@section('title')
    <title> OUR PRODUCTS || {{ env('WEBSITE_NAME') }}</title>
@endsection
@section('content')
    <div class="content-fluid">@include('components.bmenu')</div>
    @include('components.mainnav')
    <div class="productspage">
        <div class="content-fluid">
            <div class="index">
                <div class="route"><span>Home</span> / Products</div>
                <div class="title">Our Products</div>
            </div>

            @if (isset($ourproducts) && count($ourproducts) > 0)


                {{--            @dd($k)--}}
                {{--                @dd($product)--}}
                <div class="switchourproducts" data-aos="fade-up">
                    @foreach ($ourproducts as $product)

                        <div
                            class="maxi"><img @if($loop->first) data-id="switch-{{$product->id}}"
                                              @endif data-id="switch-{{$product->id}}"
                                              onclick="switched('switch-{{$product->id}}')"
                                              class=" switcheproduct @if ($loop->first)   active @endif "
                                              src="{{ env('DATA_URL') }}/product1/{{ $product->id }}.{{ $product->extension_product1}}?v={{ $product->version }}"
                                              alt=""></div>
                        {{--            <div class="ultra"><img src="/assets/svg/ultra.svg" alt=""></div>--}}
                    @endforeach
                </div>




                @foreach ($ourproducts as $product)
                    @php $k = json_decode($product->several_images)
                    @endphp
                    <div class="productdetails">
                        <div @if ($loop->first) class="prod" @endif  class="product" id="switch-{{$product->id}}" >
                            <div class="producttitle">{{ $product['productname'] }}</div>
                            <div class="productparagraph">{!! $product['productdetails'] !!}</div>
                            <div class="productrow">
                                @foreach ($k as $details)
                                    @if ($loop->index < 5)
                                        <div class="productrowimages">

                                            <div class="productimage">
                                                <img src="{{ env('DATA_URL') . $details->image}}" alt="">

                                            </div>
                                            <div class="imagetitle">
                                                {{ $details->title}}
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="productmoredetail">
                                <div class="imageleft"><img
                                        src="{{ env('DATA_URL') }}/annoted_image/{{ $product->id }}.{{ $product->extension_annoted_image}}?v={{ $product->version }}"
                                        alt=""></div>
                                <div class="imageright"><img
                                        src="{{ env('DATA_URL') }}/detailed_image/{{ $product->id }}.{{ $product->extension_detailed_image}}?v={{ $product->version }}"
                                        alt=""></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>
    @include('components.footer')
@endsection
