@extends('layouts.main')
@php error_reporting(E_ALL);
ini_set('display_errors' , 1); @endphp
@section('title')
    <title> Terms&Conditions || {{ env('WEBSITE_NAME') }}</title>
@endsection
@section('content')
    <div class="content-fluid">@include('components.bmenu')</div>
    @include('components.mainnav')
    <div class="privaciespages">
    <div class="content-fluid ">
        <div class="index">
            <div class="route"><span>Home</span> / Terms&Conditions</div>
            <div class="title">Terms&Conditions</div>
        </div>
        <div class="privacy-body">

                @if (isset($terms_conditions) && count($terms_conditions) > 0)
                    @foreach ($terms_conditions as $terms)
{{--                        @dd($terms)--}}
                        @php $term = json_decode($terms->paragraphs) @endphp
{{--                    @dd($term)--}}
                        @foreach ($term as $ter)
                            @if ($loop->index < 30)
                        <div class="paragraphs">
                            <div class="title">
                                {{ $ter->title }}
                            </div>
                            <div class="paragraph">
                                {!! $ter->paragraph!!}
                            </div>

                        </div>
                            @endif
                            @endforeach
                    @endforeach
                @endif


        </div>
    </div>
    </div>
        @include('components.footer')

@endsection
