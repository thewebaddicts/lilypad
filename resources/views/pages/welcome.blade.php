@extends('layouts.main')
@php error_reporting(E_ALL);
ini_set('display_errors' , 1); @endphp
@section('title')
    <title> HOME PAGE || {{ env('WEBSITE_NAME') }}</title>
@endsection
@section('content')
    <div class="content-fluid">@include('components.bmenu')</div>
    @include('components.mainnav')


    <div class="header-slideshow">
        {{--        <div class="swiper-slideshow">--}}
        {{--            <div class="swiper-wrapper">--}}
        @if (isset($slideshow) && count($slideshow) > 0)
            @foreach ($slideshow as $slide)


                <header class="top" style=" background-image: url('{{ $slide->image }}')">
                    <div class="topoverlay"></div>
                    <div class="content-head content-fluid ">
                        <div class="title">{{ $slide['label'] }}
                        </div>
                        <div class="sub-title">
                            {!! $slide['description'] !!}
                        </div>
                        <a href="{{ $slide['button_link'] }}">
                            <div class="head-button">
                                {{ $slide['button_text'] }}</div>
                        </a>
                    </div>
                </header>

                {{--                </div>--}}
            @endforeach
        @endif
        {{--                <div class="swiper-slide">--}}
        {{--                    <header class="top" style=" background-image: url('/assets/images/topbackground.png')" >--}}
        {{--                        <div class="topoverlay"></div>--}}
        {{--                        <div class="content-head content-fluid ">--}}
        {{--                            <div class="title">Introducing Our Newest Lilypad Pack--}}
        {{--                            </div>--}}
        {{--                            <div class="sub-title">--}}
        {{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est ultricies integer quis. Iaculis urna id volutpat lacus laoreet.--}}
        {{--                            </div>--}}
        {{--                            <a href="#">--}}
        {{--                                <div class="head-button">--}}
        {{--                                    Discover</div>--}}
        {{--                            </a>--}}
        {{--                        </div>--}}
        {{--                    </header>--}}
        {{--                </div>--}}
        {{--                <div class="swiper-slide">--}}
        {{--                    <header class="top" style=" background-image: url('/assets/images/topbackground.png')" >--}}
        {{--                        <div class="topoverlay"></div>--}}
        {{--                        <div class="content-head content-fluid ">--}}
        {{--                            <div class="title">Introducing Our Newest Lilypad Pack--}}
        {{--                            </div>--}}
        {{--                            <div class="sub-title">--}}
        {{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est ultricies integer quis. Iaculis urna id volutpat lacus laoreet.--}}
        {{--                            </div>--}}
        {{--                            <a href="#">--}}
        {{--                                <div class="head-button">--}}
        {{--                                    Discover</div>--}}
        {{--                            </a>--}}
        {{--                        </div>--}}
        {{--                    </header>--}}
        {{--                </div>--}}
    </div>

    {{--            <div class="slideshow-pagination"></div>--}}

    {{--        </div>--}}
    {{--    </div>--}}


    @include('components.about')


    <div class="home-body2">

        <div class="content-fluid">
            <div class="ourproductstitle ">
                Our Products
            </div>


            {{--            <div class="switchproducts">--}}
            {{--                @if (($products) && count($products) > 0)--}}

            {{--                    --}}{{--                    @dd(($products) && count($products) > 0)--}}
            {{--                    @foreach ($products as $products)--}}
            {{--                        @php $b = json_decode($products->several_products) @endphp--}}
            {{--                        --}}{{--                        @dd($b)--}}
            {{--                        @foreach ($b as $b)--}}
            {{--                            @if ($loop->index <2 )--}}

            {{--                                <div class="maxi" data-aos="fade-up"--}}
            {{--                                     style="color:{{ $b->title_color}}!important"> {{ $b->name }}</div>--}}
            {{--                                --}}{{--                                <div class="maxi"> {{ $b->name }}</div>--}}
            {{--                            @endif--}}
            {{--                            --}}{{--                                @if ($loop->index >2)--}}

            {{--                            --}}{{--                                    <div class="ultra"> {{ $b->name }}</div>--}}
            {{--                            --}}{{--                                    --}}{{----}}{{--                                <div class="maxi"> {{ $b->name }}</div>--}}
            {{--                            --}}{{--                                @endif--}}
            {{--                        @endforeach--}}
            {{--                    @endforeach--}}
            {{--                @endif--}}


            {{--                --}}{{--                <div class="ultra">ultrasoft</div>--}}

            {{--            </div>--}}

        </div>
        <div class="allproducts">
            <div class="body2image" data-aos="fade-up">
                <img src="/assets/images/bodtwoleft.png " data-aos="fade-up" alt="">
            </div>
            @if (($products))

                @foreach ($products as $product)

                    {{--                        @dd($products)--}}
                    @php $product = json_decode($product->several_products) @endphp

                    {{--                        @foreach ($b as $c)--}}
                    @if ($loop->index < 1)
                        @foreach ($product as $p)


                            <div class="productinfo">

                                <div class="maxi" data-aos="fade-up"
                                     style="color:{{ $p->title_color}}!important"> {{ $p->name }}</div>
                                <div class="emptyprdiv" style="width:100%"></div>
                                <div class="images" data-aos="fade-up">

                                    <img src="{{ env('DATA_URL') . $p->image1}}" alt="">
                                </div>
                                <div class="product-title" data-aos="fade-up">
                                    {{ $p->name }}
                                </div>
                                <div class="productdetails" data-aos="fade-up">
                                    {!! $p->informations!!}
                                </div>
                                <a href="{{ $p->button_link }} " style="text-decoration:none">
                                    <div class="info-button">{{ $p->button_text }} <span> <img
                                                src="/assets/svg/Icon feather-arrow-right.svg" alt=""></span></div>
                                </a>

                                <div class="detailsproductinfo" data-aos="fade-up">


                                    <div class="detailsimage" data-aos="fade-up">
                                        <img src="{{ env('DATA_URL') . $p->image2}}" alt="">
                                    </div>

                                </div>


                            </div>
                        @endforeach
                    @endif

                @endforeach
            @endif

            {{--                <div class="productinfo">--}}
            {{--                    <div class="images">--}}
            {{--                        <img src="/assets/images/maxiproduct.png" alt="">--}}
            {{--                    </div>--}}
            {{--                    <div class="product-title">--}}
            {{--                        Maxicomfort--}}
            {{--                    </div>--}}
            {{--                    <div class="productdetails">--}}
            {{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum est ultricies integer quis. Iaculis urna id volutpat lacus laoreet. Mauris vitae ultricies--}}
            {{--                    </div>--}}
            {{--                    <div class="info-button">learn more</div>--}}

            {{--                </div>--}}
            <div class="body2imageright" data-aos="fade-up">
                <img src="/assets/images/bod2imageright.png" alt="">
            </div>
        </div>

    </div>

    {{--        <div class="detailsproductinfo">--}}
    {{--          --}}




    {{--             --}}
    {{--            <div class="detailsimage">--}}
    {{--                <img src="{{ env('DATA_URL') . $pro->image2}}" alt="">--}}
    {{--            </div>--}}
    {{--           --}}
    {{--        </div>--}}


    {{--                <div class="detailsproductinfo">--}}
    {{--                    <div class="detailsimage">--}}
    {{--                        <img src="/assets/images/detailsimage.png" alt="">--}}
    {{--                    </div>--}}
    {{--                </div>--}}





    <div class="home-body3">
        @if (isset($slideshowbody) && count($slideshowbody) > 0)

            @foreach ($slideshowbody as $slidebody)
                {{--                @dd($slidebody)--}}
                <div class="body3" style=" background-image: url('{{ $slidebody->image }}')">
                    <div class="body3overlay"></div>
                    <div class="content-body3 content-fluid ">
                        <div class="title">{{ $slidebody['label'] }}
                        </div>
                        <div class="sub-title">
                            {!! $slidebody['description'] !!}
                        </div>
                        <a href="{{ $slidebody['button_link'] }}">
                            <div class="head-button">
                                {{ $slidebody['button_text'] }} </div>
                        </a>
                    </div>
                </div>
            @endforeach
        @endif

    </div>

    <div class="home-body4">
        <div class="content-fluid">
            <div class="blogstitle">
                Blogs
            </div>


            <div class="blogs">
                <div class="swiper-slideshow">
                    <div class="swiper-wrapper">

                        @foreach ($es as $e)
                            {{--                                    @if ($loop->index < 10)--}}
                            <div class="swiper-slide">
                                <div class="diffrentblogs">

                                    <div class="imageblog">
                                        <img src="{{ env('DATA_URL') . $e->image}}" alt="">
                                    </div>
                                    <div class="title">
                                        {{ $e->title }}
                                    </div>
                                    <div class="details">
                                        {!! $e->details !!}
                                    </div>
                                    <a style="text-decoration: none" href="{{ $e->button_link }}">
                                        <div class="info-button">
                                            {{ $e->button_text }}
                                            <span> <img src="/assets/svg/Icon feather-arrow-right.svg"
                                                        alt=""></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.contact')
    @include('components.footer')


















@endsection
