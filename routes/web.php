<?php

use App\Http\Controllers\CookiesPolicyController;
use App\Http\Controllers\PrivacyPolicyController;
use App\Http\Controllers\TermsAndConditionsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\BlogsPageController;
use App\Http\Controllers\ArticlesPageController;
use App\Http\Controllers\OurProductsController;
use App\Http\Controllers\AboutUsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('pages/welcome');
//})->name('home');

//Route::get('/about', function () {
//    return view('pages/about');
//})->name('about-us');

//Route::get('/product', function () {
//    return view('pages/product');
//})->name('our-products');

//Route::get('/blog', function () {
//    return view('pages/blog');
//})->name('blogs');

//Route::get('/article1', function () {
//    return view('pages/article');
//})->name('article1');

//Route::get('/contact', function () {
//    return view('pages/contactus');
//})->name('contact');

Route::get('/contact', [ContactUsController
::class, 'cinformations'])->name('contact');

Route::post('/contact', [ContactUsController::class, 'request'])->name('contact');

//Route::get('/termsandconditions', function () {
//    return view('pages/termsandconditions');
//})->name('t&c');

Route::get('/termsandconditions', [TermsAndConditionsController
::class, 'terms'])->name('t&c');

//Route::get('/privacypolicy', function () {
//    return view('pages/privacypolicy');
//})->name('p&p');

Route::get('/privacypolicy', [PrivacyPolicyController
::class, 'privacy'])->name('p&p');

//Route::get('/cookiespolicy', function () {
//    return view('pages/cookiespolicy');
//})->name('c&p');

Route::get('/cookiespolicy', [CookiesPolicyController
::class, 'cookies'])->name('c&p');

Route::get('/', [HomePageController::class, 'render'])->name('home');


Route::get('/blog', [BlogsPageController::class, 'article'])->name('blogs');

Route::get('/article1', [ArticlesPageController ::class, 'infos'])->name('article1');

Route::get('/product', [OurProductsController ::class, 'display_products'])->name('our-products');

Route::get('/about', [AboutUsController ::class, 'about'])->name('about-us');


